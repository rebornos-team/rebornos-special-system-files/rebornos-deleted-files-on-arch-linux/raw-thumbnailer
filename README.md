# raw-thumbnailer

A lightweight and fast raw image thumbnailer that can be used by file managers

https://code.google.com/archive/p/raw-thumbnailer/

How to clone this repository:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/rebornos-deleted-files-on-arch-linux/raw-thumbnailer.git
```
